var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var authApi = require('./routes/auth');
var profileApi = require('./routes/profile');
var skillsApi = require('./routes/skills');
var searchApi = require('./routes/search');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/auth', authApi);
app.use('/api/profiles', profileApi);
app.use('/api/skills', skillsApi);
app.use('/api/search', searchApi);

// everything else goes to index.html
app.use('/*', express.static(path.join(__dirname + '/public/index.html')));

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.end();
});

module.exports = app;