(function () {
    'use strict';

    describe('The profileService', function () {

        var profileService;

        beforeEach(module('app'));
        beforeEach(inject(function (_profileService_) {
            profileService = _profileService_;
        }));

        describe('Getting profiles', function () {
            it('should get my profile', function () {
                var profile;

                expect(profileService).not.toBe(null);

                profileService.get()
                    .then(function (data) {
                        profile = data;

                        expect(profile.skills).not.toBe(null);
                        expect(profile.skills.languages).not.toBe(null);
                        expect(profile.skills.platforms).not.toBe(null);
                        expect(profile.skills.tools).not.toBe(null);
                        expect(profile.skills.practices).not.toBe(null);
                    });
            })
        })
    });

})();