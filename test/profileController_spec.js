(function () {
    'use strict';

    describe('The profileController', function(){

        var $controller;

        beforeEach(module('app'));
        beforeEach(inject(function(_$controller_){
            $controller = _$controller_;
        }));

        describe('the model', function(){
            it('should show my profile', function(){
                var vm = $controller('HomeController');
                var profile = vm.profile;

                expect(vm.message).toBe('Hola!');
                expect(profile.firstName).toBe('Mike');
                expect(profile.lastName).toBe('Ibarra');
            })
        })
    });

})();