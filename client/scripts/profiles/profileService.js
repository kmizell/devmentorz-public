(function () {
    'use strict';

    module.exports = profileService;

    /* @ngInject */
    function profileService($resource) {

        var resource = $resource('/api/profiles/:id');
        var service = {
            get: get
        };

        return service;

        function get(id) {
            return resource.get({id:id}).$promise;
        }
    }
})();