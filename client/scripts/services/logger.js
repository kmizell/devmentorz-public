(function () {
    'use strict';

    module.exports = logger;

    /* @ngInject */
    function logger() {

        this.error = logError;

        function logError (err, message) {
            message |= "Unspecified error: ";
            console.error(message, err);
        }
    }
})();