(function () {
    'use strict';

    module.exports = dataservice;

    /* @ngInject */
    function dataservice($http, logger) {

        var service = {};
        service.ranks = getRanks;
        service.skills = getSkills;
        service.categories = getCategories;
        service.saveSkills = saveSkills;

        return service;

        function getSkills() {
            return $http.get('/api/skills/all')
                .then(returnResult, logger.error);
        }

        function getRanks() {
            return $http.get('/api/skills/ranks')
                .then(returnResult, logger.error);
        }

        function getCategories() {
            return $http.get('/api/skills/categories')
                .then(returnResult, logger.error);
        }

        function returnResult(result) {
            return result.data;
        }

        function saveSkills(skills) {
            return $http.post('/api/skills', skills)
                .then(function (result) {
                    console.log(result);
                }, logger.error);
        }
    }
})();