(function () {
    'use strict';

    module.exports = admin;

    /* @ngInject */
    function controller(dataService, skills) {
        var vm = this;
        vm.uncategorized = filterOnCategory(skills, 0);
        vm.languages = filterOnCategory(skills, 1);
        vm.platforms = filterOnCategory(skills, 2);
        vm.tools = filterOnCategory(skills, 3);
        vm.practices = filterOnCategory(skills, 4);

        vm.save = save;

        function save(skills, category_id) {
            var skillArray = skills.split(',');
            var category_id = category_id;

            var body = skillArray.map(function (skill) {
                var skillName = skill.trim();
                return {
                    category_id: category_id,
                    skill: skillName
                };
            });

            dataService.saveSkills(body);
        }

        function filterOnCategory(skills, category){
            return skills.filter(function(skill){
                console.log(skill);
                return skill.category_id === category;
            });
        }
    }

    /* @ngInject */
    function getAllSkills(skillsService){
        return skillsService.all()
            .then(function (result){
                return result;
            })
    }


    /* @ngInject */
    function admin($routeProvider) {
        $routeProvider
            .when('/admin', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'admin.html'
            })
            .when('/admin/categories', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'categories.html',
                resolve: {skills: getAllSkills}
            });
    }
})();