(function () {
    'use strict';

    module.exports = scoreboardBar;

    /* @ngInject */
    function scoreboardBar($routeParams, profileService, logger) {
        var directive = {
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'scoreboardBar.html',
            restrict: 'A',
            scope: {}
        };
        return directive;

        function controller() {
            var vm = this;

            activate();

            function activate() {
                profileService.get($routeParams.id),
                    loadProfile,
                    logger.error;
            }

            function loadProfile(profile) {
                vm.profile = profile;
                vm.profileName = profile.firstName + ' ' + profile.lastName;
            }
        }
    }
})();