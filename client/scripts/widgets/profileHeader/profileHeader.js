(function () {
    'use strict';

    module.exports = profileHeader;

    /* @ngInject */
    function profileHeader($routeParams, profileService){
        var directive = {
            restrict: 'A',
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'profileHeader.html',
            scope: {}
        };
        return directive;

        function controller(){
            var vm = this;

            activate();

            function activate(){
                profileService.get($routeParams.id)
                    .then(function(profile){
                        vm.profileName = profile.firstName + ' ' + profile.lastName;
                    });
            }
        }
    }
})();