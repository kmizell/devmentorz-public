(function () {
    'use strict';

    module.exports = tagfield;

    /* @ngInject */
    function tagfield(skillsService) {

        return {
            restrict: 'A',
            controller: controllerFn,
            controllerAs: 'vm',
            link: linkFn,
            template: '<input type="text" class="form-control" id="tokenfield" value=""/>',
            scope: {}
        };

        function linkFn(scope, element, attrs) {
            scope.field = element.find('input');

            var allSkills = skillsService.all()
                .then(function (data) {
                    allSkills = data.map(function (skill) {
                        return {label: skill.skill, value: skill.id};
                    });
                    $(scope.field).tokenfield({
                        autocomplete: {
                            source: allSkills,
                            delay: 100
                        },
                        showAutocompleteOnFocus: true
                    });
                });
        }

        function controllerFn($scope) {
            var vm = this;
            vm.show = function () {
                console.log($scope.field[0].value);
            }

        }
    }
})();