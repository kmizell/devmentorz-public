(function () {
    'use strict';

    module.exports = alertBar;

    /* @ngInject */
    function alertBar() {
        var directive = {
            restrict: 'A',
           controller: controller,
            controllerAs: 'ctrl',
            templateUrl: 'alertbar.html'
        };
        return directive;

        /* @ngInject */
        function controller($rootScope){
            var ctrl = this;

            ctrl.close = close;

            var scope = $rootScope;
            scope.$watch('alertMessage',function(){
                if (scope.alertMessage)
                {
                    ctrl.show = true;
                    ctrl.type = scope.alertMessage.type;
                    ctrl.message = scope.alertMessage.message;
                }
            });

            function close(){
                ctrl.show = false;
            }

            return ctrl;
        }
    }
})();