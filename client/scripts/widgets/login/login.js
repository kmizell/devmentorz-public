(function () {
    'use strict';

    module.exports = login;

    /* @ngInject */
    function login() {
        var directive = {
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'login.html',
            restrict: 'A',
            scope: {}
        };
        return directive;

        /* @ngInject */
        function controller($rootScope, $location, authenticationService) {
            var vm = this;

            vm.login = login;

            return vm;

            function login(){
                authenticationService.authenticate(this)
                    .then(success, failure);
            }

            function success(message){
                $location.url('/');
            }

            function failure(message){
                $rootScope.alertMessage = {type:'alert-danger', message: message};
            }
        }
    }
})();