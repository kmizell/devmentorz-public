(function () {
    'use strict';

    module.exports = skillsPanel;

    /* @ngInject */
    function skillsPanel($location, $routeParams) {
        var directive = {
            restrict: 'A',
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'skillsPanel.html',
            scope: {
                category: '@'
            }
        };
        return directive;

        function controller($scope) {
            var vm = this;
            vm.isEditable = $routeParams.id === 'mine';
            vm.edit = edit;
            vm.category = $scope.category;
            vm.skills = $scope.$parent.vm[vm.category.toLowerCase()];
        }

        function edit() {
            if (vm.isEditable) {
                $location.url('/edit/' + vm.category.toLowerCase());
            }
        }
    }
})();