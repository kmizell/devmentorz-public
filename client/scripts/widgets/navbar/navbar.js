(function () {
    'use strict';

    module.exports = navbar;

    /* @ngInject */
    function navbar() {
        var directive = {
            controller: controller,
            controllerAs: 'vm',
            restrict: 'A',
            templateUrl: 'navbar.html'
        };
        return directive;

        /* @ngInject */
        function controller(authenticationService){
            var vm = this;

            vm.logout = logout;

            return vm;

            function logout(){
                authenticationService.logout();
            }
        }
    }
})();