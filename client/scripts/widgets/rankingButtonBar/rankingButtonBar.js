(function () {
    'use strict';

    module.exports = rankingButtonBar;

    /* @ngInject */
    function rankingButtonBar() {
        var directive = {
            restrict: 'A',
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'rankingButtonBar.html',
            scope: {
                skill: '='
            }
        };
        return directive;
    }

    function controller($scope, skillsService) {
        var vm = this;
        vm.set = set;
        vm.isActive = isActive;
        vm.skill = $scope.skill;
        vm.ranks = [
            {rank: "No Interest", value: 0},
            {rank: "Want To Learn", value: 5},
            {rank: "Beginner", value: 10},
            {rank: "Novice", value: 15},
            {rank: "Intermediate", value: 20},
            {rank: "Advanced", value: 25},
            {rank: "Expert", value: 30}
        ];

        function set(rank) {
            var ranked_skill = {rank_id: rank.value, skill_id: vm.skill.skill_id}
            skillsService.save(ranked_skill)
                .then(function () {
                    vm.skill.rank_id = rank.value;
                    vm.skill.rank_name = rank.rank;

                });
        }

        function isActive(rank) {
            return (vm.skill.rank_id === rank.value);
        }
    }
})();