(function () {
    'use strict';

    module.exports = skillEntrybar;

    /* @ngInject */
    function skillEntrybar($routeParams, dataService, skillsService) {
        var directive = {
            restrict: 'A',
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'skillEntrybar.html',
            scope: {}

        };
        return directive;

        /* @ngInject */
        function controller() {
            var vm = this;
            var skillsHash = {};
            vm.isEditable = $routeParams.id === 'mine';
            vm.setRank = setRank;
            vm.add = add;

            activate();

            function activate() {
                dataService.ranks()
                    .then(function (ranks) {
                        vm.ranks = ranks;
                        vm.rank = vm.ranks[4];
                    });
                dataService.skills()
                    .then(function (skills) {
                        skills.forEach(function (skill) {
                            skillsHash[skill.skill.toLowerCase()] = skill.id;
                        });
                    });
            }

            function setRank(rank) {
                vm.rank = rank;
            }

            function add() {
                var ranked_skills =
                    vm.skills
                        .split(/\s*,\s*/g)
                        .map(function (skill) {
                            var savedSkill_id = skillsHash[skill.toLowerCase()];
                            var skill_id = (savedSkill_id)
                                ? savedSkill_id
                                : 0;
                            return {
                                rank_id: vm.rank.id,
                                skill: skill,
                                skill_id: skill_id
                            }
                        });

                ranked_skills.forEach(function (ranked_skill) {
                    skillsService.save(ranked_skill)
                        .then(function (result) {
                        });

                })
            }

        }
    }
})();