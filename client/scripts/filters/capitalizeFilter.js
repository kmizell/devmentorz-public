(function () {
    'use strict';

    module.exports = capitalizeFilter;

    function capitalizeFilter(){
        return function(word){
            var first = word.charAt(0).toUpperCase();
            var rest = word.substring(1).toLowerCase();
            return first + rest;
        }
    }

})();