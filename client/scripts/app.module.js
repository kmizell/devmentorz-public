(function () {
    'use strict';

    var dependencies = ['ngRoute', 'ngResource'];

    angular
        .module('app', dependencies)

        .factory('authInterceptor', require('./authentication/authInterceptor.js'))

        .service('logger', require('./services/logger.js'))
        .service('profileService', require('./profiles/profileService.js'))
        .service('skillsService', require('./skills/skillsService.js'))
        .service('authenticationService', require('./authentication/authenticationService.js'))
        .service('dataService', require('./services/dataservice.js'))

        .filter('capitalize', require('./filters/capitalizeFilter.js'))

        .directive('login', require('./widgets/login/login.js'))
        .directive('alertBar', require('./widgets/alertBar/alertBar.js'))
        .directive('navbar', require('./widgets/navbar/navbar.js'))
        .directive('skillsPanel', require('./widgets/skillsPanel/skillsPanel.js'))
        .directive('rankingButtonBar',require('./widgets/rankingButtonBar/rankingButtonBar.js'))
        .directive('scoreboardBar',require('./widgets/scoreboardBar/scoreboardBar.js'))
        .directive('tagfield',require('./widgets/tagfield.js'))
        .directive('skillEntrybar', require('./widgets/skillEntrybar/skillEntrybar.js'))
        .directive('profileHeader', require('./widgets/profileHeader/profileHeader.js'))

        .config(config)
        .config(require('./admin/admin.js'))
        .config(require('./search/search.js'))
        .config(require('./skills/skills.js'))
        .config(require('./skills/edit.js'))
        .config(require('./authentication/loginPage.js'))
        .config(require('./authentication/createAccount.js'))
        .config(require('./authentication/inviteSent.js'));

    /* @ngInject */
    function config($httpProvider, $locationProvider, $routeProvider){
        $httpProvider.interceptors.push('authInterceptor');
        $locationProvider.html5Mode(true);
        $routeProvider
            .otherwise({
                controller: function() {
                },
                templateUrl: '404.html'
            });
    }
})();