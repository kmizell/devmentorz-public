(function () {
    'use strict';

    module.exports = search;

    angular.module('app')
        .controller('SearchController', controller);

    /* @ngInject */
    function controller($http, dataService) {
        var vm = this;
        vm.search = search;

        activate();

        function activate(){
            dataService.skills()
                .then(function(data){
                    vm.skills = data;
                })
        }

        function search(){
            var query = '/api/search/' +
                '?skill=' + vm.skill +
                '&rank=' + vm.rank;

            $http.get(query)
                .then(function (data) {
                    var skillUrl = '/skills/';
                   vm.results = data.data.map(function(result){
                       result.skillsUrl = skillUrl + result.id;
                       return result;
                   });
                });
        }
    }

    /* @ngInject */
    function search($routeProvider) {
        $routeProvider
            .when('/', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'search.html'
            })
            .when('/search', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'search.html'
            });
    }
})();