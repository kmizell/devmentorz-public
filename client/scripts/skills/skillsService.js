(function () {
    'use strict';

    module.exports = skillsService;

    /* @ngInject */
    function skillsService($resource) {
        var actions = {
            all: {method: 'GET', url: '/api/skills/all', isArray: true},
            get: {method: 'GET', url: '/api/skills/:id', isArray: true},
            save: {method: 'POST', url: '/api/skills/mine', isArray: false}
        };
        var resource = $resource('/api/skills', null, actions);

        var service = {
            all: all,
            mine: mine,
            get: get,
            save: save
        };

        return service;

        function all() {
            return resource.all().$promise;
        }

        function get(id) {
            return resource.get({id:id}).$promise;
        }

        function mine() {
            return resource.get({id:'mine'}).$promise;
        }

        function save(ranked_skill) {
            return resource.save(ranked_skill).$promise;
        }
    }
})();