(function () {
    'use strict';

    module.exports = skillsConfig;

    /* @ngInject */
    function skillsController($routeParams, skillsService, $location, dataService, logger) {
        var vm = this;
        vm.languages = [];
        vm.platforms = [];
        vm.tools = [];
        vm.practices = [];
        vm.uncategorized = [];

        var skillsHash = {};
        vm.isEditable = $routeParams.id === 'mine';
        vm.setRank = setRank;
        vm.edit = edit;
        vm.add = add;

        activate();

        function setRank(rank) {
            vm.rank = rank;
        }

        function add() {
            var ranked_skills =
                vm.skills
                    .split(/\s*,\s*/g)
                    .map(function (skill) {
                        var savedSkill_id = skillsHash[skill.toLowerCase()];
                        var skill_id = (savedSkill_id)
                            ? savedSkill_id
                            : 0;
                        return {
                            rank_id: vm.rank.id,
                            skill: skill,
                            skill_id: skill_id
                        }
                    });

            ranked_skills.forEach(function (ranked_skill) {
                skillsService.save(ranked_skill)
                    .then(function (result) {
                        vm.skills = null;
                        activate();
                    });

            })
        }

        function edit(category){
            $location.url('/edit/' + category);
        }


        function activate() {
            skillsService.get($routeParams.id)
                .then(
                    function (skills) {
                        vm.languages = skills.filter(function (skill) {
                            return skill.category === 'Languages';
                        });
                        vm.tools = skills.filter(function (skill) {
                            return skill.category === 'Tools';
                        });
                        vm.practices = skills.filter(function (skill) {
                            return skill.category === 'Practices';
                        });
                        vm.platforms = skills.filter(function (skill) {
                            return skill.category === 'Platforms';
                        });
                        vm.uncategorized = skills.filter(function (skill) {
                            return skill.category === 'Uncategorized';
                        });
                    },
                    logger.error);

            dataService.ranks()
                .then(function (ranks) {
                    vm.ranks = ranks;
                    vm.rank = vm.ranks[4];
                });
            dataService.skills()
                .then(function (skills) {
                    skills.forEach(function (skill) {
                        skillsHash[skill.skill.toLowerCase()] = skill.id;
                    });
                });

        }
    }

    /* @ngInject */
    function skillsConfig($routeProvider) {
        $routeProvider
            .when('/skills/:id', {
                controller: skillsController,
                controllerAs: 'vm',
                templateUrl: 'skills.html'
            });
    }

})();