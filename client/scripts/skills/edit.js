(function () {
    'use strict';

    module.exports = edit;

    angular.module('app')
        .controller('SkillsEditController', controller);

    /* @ngInject */
    function controller($routeParams, skills, mySkills) {
        var vm = this;
        vm.category = $routeParams.skill_category;
        var filteredSkills = filterOnCategory(skills,vm.category);
        vm.mySkills = filterOnCategory(mySkills, vm.category);
    }

    /* @ngInject */
    function edit($routeProvider) {
        $routeProvider
            .when('/edit/:skill_category', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'edit.html',
                resolve: {
                    skills: resolveSkills,
                   mySkills: resolveMySkills
                }
            });
    }

    function resolveSkills(skillsService){
        return skillsService.all()
            .then(function(items){
                return items
            });
    }

    function resolveMySkills(skillsService){
        return skillsService.mine()
            .then(function(items){
                return items
            });
    }

    function filterOnCategory(skills, category){
        return skills.filter(function(skill){
            return skill.category.toLowerCase() === category.toLowerCase();
        });
    }
})();