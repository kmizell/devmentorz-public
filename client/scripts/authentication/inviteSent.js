(function () {
    'use strict';

    module.exports = inviteSent;

    angular
        .module('app')
        .controller('InviteSentController', controller);

    /* @ngInject */
    function controller() {
        var vm = this;
    }

    /* @ngInject */
    function inviteSent($routeProvider) {
        $routeProvider
            .when('/inviteSent', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'inviteSent.html'
            });
    }
})();