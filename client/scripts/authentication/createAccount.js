(function () {
    'use strict';

    module.exports = createAccount;

    angular.module('app')
        .controller('AccountController', controller);

    /* @ngInject */
    function controller($location, $routeParams, authenticationService) {
        var vm = this;

        vm.create = create;
        vm.confirm = confirm;

        function create() {
            authenticationService.create(vm)
                .then(function () {
                    $location.url('/inviteSent');
                });
        }

        function confirm(){
            var credentials = {
                token: $routeParams.token,
                firstName: vm.firstName,
                lastName: vm.lastName,
                email: vm.email,
                password: vm.password
            };

            authenticationService.confirm(credentials)
                .then(function () {
                    $location.url('/login');
                });
        }
    }

    /* @ngInject */
    function createAccount($routeProvider) {
        $routeProvider
            .when('/createAccount', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'createAccount.html'
            })
            .when('/confirmAccount/:token', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'confirmAccount.html'
            });
    }
})();