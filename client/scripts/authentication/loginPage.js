(function () {
    'use strict';

    module.exports = loginPage;

    angular.module('app')
        .controller('LoginController', controller);

    /* @ngInject */
    function controller() {
        var vm = this;
    }

    /* @ngInject */
    function loginPage($routeProvider) {
        $routeProvider
            .when('/login', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'loginPage.html'
            });
    }
})();