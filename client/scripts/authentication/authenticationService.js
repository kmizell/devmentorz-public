(function () {
    'use strict';

    module.exports = authenticationService;

    /* @ngInject */
    function authenticationService($http, $q, $route) {

        var baseUrl = '/api/auth';
        var service = {
            authenticate: authenticate,
            create: create,
            confirm: confirm,
            logout: logout
        };

        return service;

        function authenticate(credentials) {
            var url = baseUrl + "/login";
            return $http.post(url, credentials)
                .then(success, failure)
        }

        function create(email) {
            var url = baseUrl + "/getEnrollmentToken";
            return $http.post(url, email);
        }

        function confirm(credentials){
            var url = baseUrl + "/enrollNewUser";
            return $http.post(url, credentials);
        }

        function logout(){
            var url = baseUrl + "/logout"
            return $http.get(url)
                .then(function(){
                    $route.reload();
                })
        }

        function success(response) {
            var deferred = $q.defer();

            if (response.status === 200) {
                deferred.resolve(response.data);
            } else {
                deferred.reject(response.data);
            }

            return deferred.promise;
        }

        function failure(response) {
            var deferred = $q.defer();
            if (response.status === 401) {
                deferred.reject(response.data);
            } else {
                deferred.reject("There was an error while validating your credentials.");
            }

            return deferred.promise;
        }
    }
})();