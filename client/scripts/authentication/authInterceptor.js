(function () {
    'use strict';

    module.exports = authInterceptor;

    /* @ngInject */
    function authInterceptor($rootScope, $location) {
        var service = {
            request: request,
            requestError: requestError,
            response: response,
            responseError: responseError
        };

        return service;

        function request(config) {
            return config
        }
        function requestError(config) {
            return config
        }
        function response(response) {
            return response
        }
        function responseError(response) {
            var status = response.status;
            if (status === 401){
                var message = "Please log in to your account."
                $rootScope.alertMessage = {type:'alert-info', message: message};
                $location.url('/login');
            }
            if (status === 404){
                $location.url('/404');
            }
            return response;
        }
    }
})();