(function () {
    'use strict';

    module.exports = handleError;

    function handleError(error, res){
        console.error(JSON.stringify(error));
        if (error.code === '23505') {
            res.status(409)
                .send('Email already exists')
        }
        else {
            res.status(500)
                .send(error);
        }
    }

})();