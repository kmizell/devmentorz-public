(function () {
    'use strict';

    var gulp = require('gulp'),
        browserify = require('gulp-browserify'),
        transform = require('vinyl-transform'),
        templateCache = require('gulp-angular-templatecache'),
        rename = require('gulp-rename'),
        del = require('del'),
        Server = require('karma').Server;

    var paths = {
        templates_src: ['./client/scripts/**/*.html'],
        scripts_src: ['./client/scripts/**/*.js'],
        styles_src: ['./bower_components/**/*.min.css', './client/styles/**/*.css'],
        fonts_src: ['./bower_components/font-awesome/fonts/*'],
        js_lib_src: ['./bower_components/**/*.min.js', './bower_components/**/angular-mocks.js'],
        spec_src: ['./test/**/*_spec.js'],

        scripts_dest: './public/js/',
        styles_dest: './public/css/',
        fonts_dest: './public/fonts/'
    };

    gulp.task('clean', _clean);
    gulp.task('browserify', ['scripts'], _browserify);
    gulp.task('scripts', _scripts);
    gulp.task('styles', _styles);
    gulp.task('fonts', _fonts);
    gulp.task('templates', _templates);
    gulp.task('build', ['clean', 'templates', 'scripts', 'browserify', 'styles', 'fonts']);
    gulp.task('watch', ['build'], _watch);
    gulp.task('test', ['watch'], _karma);
    gulp.task('default', ['watch']);

    function _clean() {
        return del.sync([paths.scripts_dest, paths.styles_dest]);
    }

    function _browserify() {
        return gulp.src('./client/scripts/app.module.js')
            .pipe(browserify())
            .pipe(rename('app.js'))
            .pipe(gulp.dest(paths.scripts_dest));
    }

    function _templates() {
        return gulp.src(paths.templates_src)
            .pipe(templateCache({module: 'app', transformUrl: transformTemplateUrl}))
            .pipe(gulp.dest(paths.scripts_dest));
    }

    function _scripts() {
        return gulp.src(paths.js_lib_src)
            .pipe(rename({dirname: ''}))
            .pipe(gulp.dest(paths.scripts_dest));
    }

    function _styles() {
        return gulp.src(paths.styles_src)
            .pipe(rename({dirname: ''}))
            .pipe(gulp.dest(paths.styles_dest))
    }

    function _fonts() {
        return gulp.src(paths.fonts_src)
            .pipe(gulp.dest(paths.fonts_dest))
    }

    function _watch() {
        gulp.watch(paths.templates_src, ['templates']);
        gulp.watch(paths.scripts_src, ['browserify']);
        gulp.watch(paths.styles_src, ['styles']);
    }

    function _karma(done) {
        new Server({
            configFile: __dirname + '/karma.conf.js',
            singleRun: false
        }, done).start();
    }

    function transformTemplateUrl(url){
        return url.match(/(([a-zA-Z0-9]+).html)/)[0];
    }

})();
