(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = admin;

    /* @ngInject */
    function controller(dataService, skills) {
        var vm = this;
        vm.uncategorized = filterOnCategory(skills, 0);
        vm.languages = filterOnCategory(skills, 1);
        vm.platforms = filterOnCategory(skills, 2);
        vm.tools = filterOnCategory(skills, 3);
        vm.practices = filterOnCategory(skills, 4);

        vm.save = save;

        function save(skills, category_id) {
            var skillArray = skills.split(',');
            var category_id = category_id;

            var body = skillArray.map(function (skill) {
                var skillName = skill.trim();
                return {
                    category_id: category_id,
                    skill: skillName
                };
            });

            dataService.saveSkills(body);
        }

        function filterOnCategory(skills, category){
            return skills.filter(function(skill){
                console.log(skill);
                return skill.category_id === category;
            });
        }
    }

    /* @ngInject */
    function getAllSkills(skillsService){
        return skillsService.all()
            .then(function (result){
                return result;
            })
    }


    /* @ngInject */
    function admin($routeProvider) {
        $routeProvider
            .when('/admin', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'admin.html'
            })
            .when('/admin/categories', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'categories.html',
                resolve: {skills: getAllSkills}
            });
    }
})();
},{}],2:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = authInterceptor;

    /* @ngInject */
    function authInterceptor($rootScope, $location) {
        var service = {
            request: request,
            requestError: requestError,
            response: response,
            responseError: responseError
        };

        return service;

        function request(config) {
            return config
        }
        function requestError(config) {
            return config
        }
        function response(response) {
            return response
        }
        function responseError(response) {
            var status = response.status;
            if (status === 401){
                var message = "Please log in to your account."
                $rootScope.alertMessage = {type:'alert-info', message: message};
                $location.url('/login');
            }
            if (status === 404){
                $location.url('/404');
            }
            return response;
        }
    }
})();
},{}],3:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = authenticationService;

    /* @ngInject */
    function authenticationService($http, $q, $route) {

        var baseUrl = '/api/auth';
        var service = {
            authenticate: authenticate,
            create: create,
            confirm: confirm,
            logout: logout
        };

        return service;

        function authenticate(credentials) {
            var url = baseUrl + "/login";
            return $http.post(url, credentials)
                .then(success, failure)
        }

        function create(email) {
            var url = baseUrl + "/getEnrollmentToken";
            return $http.post(url, email);
        }

        function confirm(credentials){
            var url = baseUrl + "/enrollNewUser";
            return $http.post(url, credentials);
        }

        function logout(){
            var url = baseUrl + "/logout"
            return $http.get(url)
                .then(function(){
                    $route.reload();
                })
        }

        function success(response) {
            var deferred = $q.defer();

            if (response.status === 200) {
                deferred.resolve(response.data);
            } else {
                deferred.reject(response.data);
            }

            return deferred.promise;
        }

        function failure(response) {
            var deferred = $q.defer();
            if (response.status === 401) {
                deferred.reject(response.data);
            } else {
                deferred.reject("There was an error while validating your credentials.");
            }

            return deferred.promise;
        }
    }
})();
},{}],4:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = createAccount;

    angular.module('app')
        .controller('AccountController', controller);

    /* @ngInject */
    function controller($location, $routeParams, authenticationService) {
        var vm = this;

        vm.create = create;
        vm.confirm = confirm;

        function create() {
            authenticationService.create(vm)
                .then(function () {
                    $location.url('/inviteSent');
                });
        }

        function confirm(){
            var credentials = {
                token: $routeParams.token,
                firstName: vm.firstName,
                lastName: vm.lastName,
                email: vm.email,
                password: vm.password
            };

            authenticationService.confirm(credentials)
                .then(function () {
                    $location.url('/login');
                });
        }
    }

    /* @ngInject */
    function createAccount($routeProvider) {
        $routeProvider
            .when('/createAccount', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'createAccount.html'
            })
            .when('/confirmAccount/:token', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'confirmAccount.html'
            });
    }
})();
},{}],5:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = inviteSent;

    angular
        .module('app')
        .controller('InviteSentController', controller);

    /* @ngInject */
    function controller() {
        var vm = this;
    }

    /* @ngInject */
    function inviteSent($routeProvider) {
        $routeProvider
            .when('/inviteSent', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'inviteSent.html'
            });
    }
})();
},{}],6:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = loginPage;

    angular.module('app')
        .controller('LoginController', controller);

    /* @ngInject */
    function controller() {
        var vm = this;
    }

    /* @ngInject */
    function loginPage($routeProvider) {
        $routeProvider
            .when('/login', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'loginPage.html'
            });
    }
})();
},{}],7:[function(require,module,exports){
(function () {
    'use strict';

    var dependencies = ['ngRoute', 'ngResource'];

    angular
        .module('app', dependencies)

        .factory('authInterceptor', require('./authentication/authInterceptor.js'))

        .service('logger', require('./services/logger.js'))
        .service('profileService', require('./profiles/profileService.js'))
        .service('skillsService', require('./skills/skillsService.js'))
        .service('authenticationService', require('./authentication/authenticationService.js'))
        .service('dataService', require('./services/dataservice.js'))

        .filter('capitalize', require('./filters/capitalizeFilter.js'))

        .directive('login', require('./widgets/login/login.js'))
        .directive('alertBar', require('./widgets/alertBar/alertBar.js'))
        .directive('navbar', require('./widgets/navbar/navbar.js'))
        .directive('skillsPanel', require('./widgets/skillsPanel/skillsPanel.js'))
        .directive('rankingButtonBar',require('./widgets/rankingButtonBar/rankingButtonBar.js'))
        .directive('scoreboardBar',require('./widgets/scoreboardBar/scoreboardBar.js'))
        .directive('tagfield',require('./widgets/tagfield.js'))
        .directive('skillEntrybar', require('./widgets/skillEntrybar/skillEntrybar.js'))
        .directive('profileHeader', require('./widgets/profileHeader/profileHeader.js'))

        .config(config)
        .config(require('./admin/admin.js'))
        .config(require('./search/search.js'))
        .config(require('./skills/skills.js'))
        .config(require('./skills/edit.js'))
        .config(require('./authentication/loginPage.js'))
        .config(require('./authentication/createAccount.js'))
        .config(require('./authentication/inviteSent.js'));

    /* @ngInject */
    function config($httpProvider, $locationProvider, $routeProvider){
        $httpProvider.interceptors.push('authInterceptor');
        $locationProvider.html5Mode(true);
        $routeProvider
            .otherwise({
                controller: function() {
                },
                templateUrl: '404.html'
            });
    }
})();
},{"./admin/admin.js":1,"./authentication/authInterceptor.js":2,"./authentication/authenticationService.js":3,"./authentication/createAccount.js":4,"./authentication/inviteSent.js":5,"./authentication/loginPage.js":6,"./filters/capitalizeFilter.js":8,"./profiles/profileService.js":9,"./search/search.js":10,"./services/dataservice.js":11,"./services/logger.js":12,"./skills/edit.js":13,"./skills/skills.js":14,"./skills/skillsService.js":15,"./widgets/alertBar/alertBar.js":16,"./widgets/login/login.js":17,"./widgets/navbar/navbar.js":18,"./widgets/profileHeader/profileHeader.js":19,"./widgets/rankingButtonBar/rankingButtonBar.js":20,"./widgets/scoreboardBar/scoreboardBar.js":21,"./widgets/skillEntrybar/skillEntrybar.js":22,"./widgets/skillsPanel/skillsPanel.js":23,"./widgets/tagfield.js":24}],8:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = capitalizeFilter;

    function capitalizeFilter(){
        return function(word){
            var first = word.charAt(0).toUpperCase();
            var rest = word.substring(1).toLowerCase();
            return first + rest;
        }
    }

})();
},{}],9:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = profileService;

    /* @ngInject */
    function profileService($resource) {

        var resource = $resource('/api/profiles/:id');
        var service = {
            get: get
        };

        return service;

        function get(id) {
            return resource.get({id:id}).$promise;
        }
    }
})();
},{}],10:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = search;

    angular.module('app')
        .controller('SearchController', controller);

    /* @ngInject */
    function controller($http, dataService) {
        var vm = this;
        vm.search = search;

        activate();

        function activate(){
            dataService.skills()
                .then(function(data){
                    vm.skills = data;
                })
        }

        function search(){
            var query = '/api/search/' +
                '?skill=' + vm.skill +
                '&rank=' + vm.rank;

            $http.get(query)
                .then(function (data) {
                    var skillUrl = '/skills/';
                   vm.results = data.data.map(function(result){
                       result.skillsUrl = skillUrl + result.id;
                       return result;
                   });
                });
        }
    }

    /* @ngInject */
    function search($routeProvider) {
        $routeProvider
            .when('/', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'search.html'
            })
            .when('/search', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'search.html'
            });
    }
})();
},{}],11:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = dataservice;

    /* @ngInject */
    function dataservice($http, logger) {

        var service = {};
        service.ranks = getRanks;
        service.skills = getSkills;
        service.categories = getCategories;
        service.saveSkills = saveSkills;

        return service;

        function getSkills() {
            return $http.get('/api/skills/all')
                .then(returnResult, logger.error);
        }

        function getRanks() {
            return $http.get('/api/skills/ranks')
                .then(returnResult, logger.error);
        }

        function getCategories() {
            return $http.get('/api/skills/categories')
                .then(returnResult, logger.error);
        }

        function returnResult(result) {
            return result.data;
        }

        function saveSkills(skills) {
            return $http.post('/api/skills', skills)
                .then(function (result) {
                    console.log(result);
                }, logger.error);
        }
    }
})();
},{}],12:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = logger;

    /* @ngInject */
    function logger() {

        this.error = logError;

        function logError (err, message) {
            message |= "Unspecified error: ";
            console.error(message, err);
        }
    }
})();
},{}],13:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = edit;

    angular.module('app')
        .controller('SkillsEditController', controller);

    /* @ngInject */
    function controller($routeParams, skills, mySkills) {
        var vm = this;
        vm.category = $routeParams.skill_category;
        var filteredSkills = filterOnCategory(skills,vm.category);
        vm.mySkills = filterOnCategory(mySkills, vm.category);
    }

    /* @ngInject */
    function edit($routeProvider) {
        $routeProvider
            .when('/edit/:skill_category', {
                controller: controller,
                controllerAs: 'vm',
                templateUrl: 'edit.html',
                resolve: {
                    skills: resolveSkills,
                   mySkills: resolveMySkills
                }
            });
    }

    function resolveSkills(skillsService){
        return skillsService.all()
            .then(function(items){
                return items
            });
    }

    function resolveMySkills(skillsService){
        return skillsService.mine()
            .then(function(items){
                return items
            });
    }

    function filterOnCategory(skills, category){
        return skills.filter(function(skill){
            return skill.category.toLowerCase() === category.toLowerCase();
        });
    }
})();
},{}],14:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = skillsConfig;

    /* @ngInject */
    function skillsController($routeParams, skillsService, $location, dataService, logger) {
        var vm = this;
        vm.languages = [];
        vm.platforms = [];
        vm.tools = [];
        vm.practices = [];
        vm.uncategorized = [];

        var skillsHash = {};
        vm.isEditable = $routeParams.id === 'mine';
        vm.setRank = setRank;
        vm.edit = edit;
        vm.add = add;

        activate();

        function setRank(rank) {
            vm.rank = rank;
        }

        function add() {
            var ranked_skills =
                vm.skills
                    .split(/\s*,\s*/g)
                    .map(function (skill) {
                        var savedSkill_id = skillsHash[skill.toLowerCase()];
                        var skill_id = (savedSkill_id)
                            ? savedSkill_id
                            : 0;
                        return {
                            rank_id: vm.rank.id,
                            skill: skill,
                            skill_id: skill_id
                        }
                    });

            ranked_skills.forEach(function (ranked_skill) {
                skillsService.save(ranked_skill)
                    .then(function (result) {
                        vm.skills = null;
                        activate();
                    });

            })
        }

        function edit(category){
            $location.url('/edit/' + category);
        }


        function activate() {
            skillsService.get($routeParams.id)
                .then(
                    function (skills) {
                        vm.languages = skills.filter(function (skill) {
                            return skill.category === 'Languages';
                        });
                        vm.tools = skills.filter(function (skill) {
                            return skill.category === 'Tools';
                        });
                        vm.practices = skills.filter(function (skill) {
                            return skill.category === 'Practices';
                        });
                        vm.platforms = skills.filter(function (skill) {
                            return skill.category === 'Platforms';
                        });
                        vm.uncategorized = skills.filter(function (skill) {
                            return skill.category === 'Uncategorized';
                        });
                    },
                    logger.error);

            dataService.ranks()
                .then(function (ranks) {
                    vm.ranks = ranks;
                    vm.rank = vm.ranks[4];
                });
            dataService.skills()
                .then(function (skills) {
                    skills.forEach(function (skill) {
                        skillsHash[skill.skill.toLowerCase()] = skill.id;
                    });
                });

        }
    }

    /* @ngInject */
    function skillsConfig($routeProvider) {
        $routeProvider
            .when('/skills/:id', {
                controller: skillsController,
                controllerAs: 'vm',
                templateUrl: 'skills.html'
            });
    }

})();
},{}],15:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = skillsService;

    /* @ngInject */
    function skillsService($resource) {
        var actions = {
            all: {method: 'GET', url: '/api/skills/all', isArray: true},
            get: {method: 'GET', url: '/api/skills/:id', isArray: true},
            save: {method: 'POST', url: '/api/skills/mine', isArray: false}
        };
        var resource = $resource('/api/skills', null, actions);

        var service = {
            all: all,
            mine: mine,
            get: get,
            save: save
        };

        return service;

        function all() {
            return resource.all().$promise;
        }

        function get(id) {
            return resource.get({id:id}).$promise;
        }

        function mine() {
            return resource.get({id:'mine'}).$promise;
        }

        function save(ranked_skill) {
            return resource.save(ranked_skill).$promise;
        }
    }
})();
},{}],16:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = alertBar;

    /* @ngInject */
    function alertBar() {
        var directive = {
            restrict: 'A',
           controller: controller,
            controllerAs: 'ctrl',
            templateUrl: 'alertbar.html'
        };
        return directive;

        /* @ngInject */
        function controller($rootScope){
            var ctrl = this;

            ctrl.close = close;

            var scope = $rootScope;
            scope.$watch('alertMessage',function(){
                if (scope.alertMessage)
                {
                    ctrl.show = true;
                    ctrl.type = scope.alertMessage.type;
                    ctrl.message = scope.alertMessage.message;
                }
            });

            function close(){
                ctrl.show = false;
            }

            return ctrl;
        }
    }
})();
},{}],17:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = login;

    /* @ngInject */
    function login() {
        var directive = {
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'login.html',
            restrict: 'A',
            scope: {}
        };
        return directive;

        /* @ngInject */
        function controller($rootScope, $location, authenticationService) {
            var vm = this;

            vm.login = login;

            return vm;

            function login(){
                authenticationService.authenticate(this)
                    .then(success, failure);
            }

            function success(message){
                $location.url('/');
            }

            function failure(message){
                $rootScope.alertMessage = {type:'alert-danger', message: message};
            }
        }
    }
})();
},{}],18:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = navbar;

    /* @ngInject */
    function navbar() {
        var directive = {
            controller: controller,
            controllerAs: 'vm',
            restrict: 'A',
            templateUrl: 'navbar.html'
        };
        return directive;

        /* @ngInject */
        function controller(authenticationService){
            var vm = this;

            vm.logout = logout;

            return vm;

            function logout(){
                authenticationService.logout();
            }
        }
    }
})();
},{}],19:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = profileHeader;

    /* @ngInject */
    function profileHeader($routeParams, profileService){
        var directive = {
            restrict: 'A',
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'profileHeader.html',
            scope: {}
        };
        return directive;

        function controller(){
            var vm = this;

            activate();

            function activate(){
                profileService.get($routeParams.id)
                    .then(function(profile){
                        vm.profileName = profile.firstName + ' ' + profile.lastName;
                    });
            }
        }
    }
})();
},{}],20:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = rankingButtonBar;

    /* @ngInject */
    function rankingButtonBar() {
        var directive = {
            restrict: 'A',
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'rankingButtonBar.html',
            scope: {
                skill: '='
            }
        };
        return directive;
    }

    function controller($scope, skillsService) {
        var vm = this;
        vm.set = set;
        vm.isActive = isActive;
        vm.skill = $scope.skill;
        vm.ranks = [
            {rank: "No Interest", value: 0},
            {rank: "Want To Learn", value: 5},
            {rank: "Beginner", value: 10},
            {rank: "Novice", value: 15},
            {rank: "Intermediate", value: 20},
            {rank: "Advanced", value: 25},
            {rank: "Expert", value: 30}
        ];

        function set(rank) {
            var ranked_skill = {rank_id: rank.value, skill_id: vm.skill.skill_id}
            skillsService.save(ranked_skill)
                .then(function () {
                    vm.skill.rank_id = rank.value;
                    vm.skill.rank_name = rank.rank;

                });
        }

        function isActive(rank) {
            return (vm.skill.rank_id === rank.value);
        }
    }
})();
},{}],21:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = scoreboardBar;

    /* @ngInject */
    function scoreboardBar($routeParams, profileService, logger) {
        var directive = {
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'scoreboardBar.html',
            restrict: 'A',
            scope: {}
        };
        return directive;

        function controller() {
            var vm = this;

            activate();

            function activate() {
                profileService.get($routeParams.id),
                    loadProfile,
                    logger.error;
            }

            function loadProfile(profile) {
                vm.profile = profile;
                vm.profileName = profile.firstName + ' ' + profile.lastName;
            }
        }
    }
})();
},{}],22:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = skillEntrybar;

    /* @ngInject */
    function skillEntrybar($routeParams, dataService, skillsService) {
        var directive = {
            restrict: 'A',
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'skillEntrybar.html',
            scope: {}

        };
        return directive;

        /* @ngInject */
        function controller() {
            var vm = this;
            var skillsHash = {};
            vm.isEditable = $routeParams.id === 'mine';
            vm.setRank = setRank;
            vm.add = add;

            activate();

            function activate() {
                dataService.ranks()
                    .then(function (ranks) {
                        vm.ranks = ranks;
                        vm.rank = vm.ranks[4];
                    });
                dataService.skills()
                    .then(function (skills) {
                        skills.forEach(function (skill) {
                            skillsHash[skill.skill.toLowerCase()] = skill.id;
                        });
                    });
            }

            function setRank(rank) {
                vm.rank = rank;
            }

            function add() {
                var ranked_skills =
                    vm.skills
                        .split(/\s*,\s*/g)
                        .map(function (skill) {
                            var savedSkill_id = skillsHash[skill.toLowerCase()];
                            var skill_id = (savedSkill_id)
                                ? savedSkill_id
                                : 0;
                            return {
                                rank_id: vm.rank.id,
                                skill: skill,
                                skill_id: skill_id
                            }
                        });

                ranked_skills.forEach(function (ranked_skill) {
                    skillsService.save(ranked_skill)
                        .then(function (result) {
                        });

                })
            }

        }
    }
})();
},{}],23:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = skillsPanel;

    /* @ngInject */
    function skillsPanel($location, $routeParams) {
        var directive = {
            restrict: 'A',
            controller: controller,
            controllerAs: 'vm',
            templateUrl: 'skillsPanel.html',
            scope: {
                category: '@'
            }
        };
        return directive;

        function controller($scope) {
            var vm = this;
            vm.isEditable = $routeParams.id === 'mine';
            vm.edit = edit;
            vm.category = $scope.category;
            vm.skills = $scope.$parent.vm[vm.category.toLowerCase()];
        }

        function edit() {
            if (vm.isEditable) {
                $location.url('/edit/' + vm.category.toLowerCase());
            }
        }
    }
})();
},{}],24:[function(require,module,exports){
(function () {
    'use strict';

    module.exports = tagfield;

    /* @ngInject */
    function tagfield(skillsService) {

        return {
            restrict: 'A',
            controller: controllerFn,
            controllerAs: 'vm',
            link: linkFn,
            template: '<input type="text" class="form-control" id="tokenfield" value=""/>',
            scope: {}
        };

        function linkFn(scope, element, attrs) {
            scope.field = element.find('input');

            var allSkills = skillsService.all()
                .then(function (data) {
                    allSkills = data.map(function (skill) {
                        return {label: skill.skill, value: skill.id};
                    });
                    $(scope.field).tokenfield({
                        autocomplete: {
                            source: allSkills,
                            delay: 100
                        },
                        showAutocompleteOnFocus: true
                    });
                });
        }

        function controllerFn($scope) {
            var vm = this;
            vm.show = function () {
                console.log($scope.field[0].value);
            }

        }
    }
})();
},{}]},{},[7])