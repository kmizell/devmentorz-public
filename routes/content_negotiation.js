(function () {
    'use strict';

    module.exports = function negotiate(req, res, next){
        var mimeTypes = ['text/json','application/json'];

        if (!req.accepts(mimeTypes)) {
            res.status(406).send('Accept Header must be JSON.').end();
            return;
        }

        if (req.method !== 'GET' && req.method !== 'DELETE' && !req.is('json')){
            res.status(406).send('Content-Type must be JSON.').end();
            return;
        }
        next();
    };

})();