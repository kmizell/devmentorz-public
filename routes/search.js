(function () {
    'use strict';

    var negotiate = require('./content_negotiation');
    var authenticate = require('./authentication');
    var router = require('express').Router();
    var handleError = require('../common/errorHandler');

    var Search = require('../data/Search');

    var profiles = new Search();

    module.exports = router;

    router.route('/')
        .all(authenticate)
        .all(negotiate)
        .get(getProfile);

    function getProfile(req, res) {
        if (!req.query.skill || !req.query.rank) {
            res
                .status(400)
                .send("Skill and Rank are both required parameters");
        }

        var query = {
            skill: req.query.skill,
            rank: req.query.rank
        };

        profiles.query(query)
            .then(function (data) {
                var mapped = data.map(function (result) {
                    return {
                        id: result.profile_id,
                        firstname: result.firstname,
                        lastname: result.lastname,
                        skill: {
                            id: result.skill_id,
                            skill: result.skill,
                            category: result.category
                        },
                        rank: {
                            id: result.rank_id,
                            rank: result.rank
                        }
                    }
                })
                res
                    .status(200)
                    .send(mapped);
            }, handleError)

    }
})();