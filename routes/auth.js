(function () {
    'use strict';

    var negotiate = require('./content_negotiation');
    var crypto = require('crypto');
    var router = require('express').Router();
    var handleError = require('../common/errorHandler');

    var AppUsers = require('../data/AppUsers');
    var AppTokens = require('../data/AppTokens');
    var Profiles = require('../data/Profiles');
    var MailService = require('../mailservice/MailService');

    var users = new AppUsers();
    var tokens = new AppTokens();
    var profiles = new Profiles();
    var mail = new MailService();

    var appName = 'mentor';

    module.exports = router;

    router.route('/login')
        .all(negotiate)
        .post(login);

    router.route('/logout')
        .all(negotiate)
        .get(logout);

    router.route('/createUser')
        .all(negotiate)
        .post(createUser);

    router.route('/getResetToken')
        .all(negotiate)
        .post(getResetToken);

    router.route('/resetPassword')
        .all(negotiate)
        .post(resetPassword);

    router.route('/inviteUser')
        .all(negotiate)
        .post(inviteUser);

    router.route('/getEnrollmentToken')
        .all(negotiate)
        .post(getEnrollmentToken);

    router.route('/enrollNewUser')
        .all(negotiate)
        .post(enrollNewUser);

    /*===================================================*/

    function login(req, res) {
        var email = req.body.email;
        var password = req.body.password;

        users.get(
            {
                email: email,
                app: appName
            })
            .then(
                function (data) {
                    var user = data.pop();
                    validatePassword(user, password, res);
                },
                function (error) {
                    handleError(error, res)
                });
    }

    function logout(req, res) {
        res
            .clearCookie('auth')
            .status(200)
            .end();
    }

    function createProfile(req, res){
        var user = {
            id: req.body.id,
            firstname: req.body.firstName,
            lastname: req.body.lastName
        }

        profiles.create(user)
            .then(function(data){
                    res.status(202).send({userId:data.id});
                },
                function(error){
                    handleError(error, res);
                });
    }

    function createUser(req, res) {
        var salt = generateToken();
        var user = {
            app: appName,
            email: req.body.email,
            passwordsalt: salt,
            password: hashPassword(req.body.password, salt)
        };
        users.create(user)
            .then(
                function (data) {
                    req.body.id = data.pop().id;
                    createProfile(req, res);
                },
                function (error) {
                    handleError(error, res);
                });
    }

    function inviteUser(req, res) {
        var password = generateToken();
        var salt = generateToken();
        var expiration = new Date(Date.now() + 1000 * 60 * 60 * 24);
        var user = {
            app: appName,
            email: req.body.email,
            passwordsalt: salt,
            password: hashPassword(password, salt)
        };
        users.create(user)
            .then(
                function (data) {
                    var query = {
                        email: data.pop().email,
                        token: generateToken(),
                        expires: expiration,
                        app: appName
                    };
                    tokens.create(query)
                        .then(
                            function (data) {
                                var body = data.pop();
                                mail.sendInvite(body);
                                res.status(202).send({
                                    email: body.email,
                                    token: body.token,
                                    expires: body.expires
                                });
                            });
                },
                function (error) {
                    handleError(error, res);
                });
    }

    function getEnrollmentToken(req, res) {

        var token = {
            email: req.body.email,
            app: appName,
            token: generateToken(),
            expires: new Date(Date.now() + 1000 * 60 * 60 * 24)
        };
        tokens.create(token)
            .then(
                function (data) {
                    var body = data.pop();
                    mail.sendInvite(token);
                    res.status(202).send({
                        email: body.email,
                        token: body.token,
                        expires: body.expires
                    });
                }, function (error) {
                    handleError(error, res);
                });
    }

    function enrollNewUser(req, res) {
        var token = {
            email: req.body.email,
            app: appName,
            token: req.body.token,
            password: req.body.password
        };

        return tokens.validate(token)
            .then(function(isValid){
                if (isValid){
                    tokens.delete(token).then();
                    return createUser(req, res);
                }
                else {
                    res.status(401).send('token is not valid');
                }
            }, function(error){
                handleError(error, res);
            });
    }

    function getResetToken(req, res) {

        var query = {
            email: req.body.email,
            app: appName,
            token: generateToken(),
            expires: new Date(Date.now() + 1000 * 60 * 60 * 24)
        };

        users.get(query)
            .then(function (data) {
                if (data.length === 0) {
                    res.status(202).send({
                        email: query.email,
                        token: query.token,
                        expires: query.expires
                    });
                }
                else {
                    tokens.create(query)
                        .then(
                            function (data) {
                                var body = data.pop();
                                res.status(202).send({
                                    email: body.email,
                                    token: body.token,
                                    expires: body.expires
                                });
                            });
                }
            }, function (error) {
                handleError(error, res);
            });
    }

    function resetPassword(req, res) {
        var query = {
            token: req.body.token,
            email: req.body.email,
            password: req.body.password,
            app: appName
        };
        tokens.validate(query)
            .then(function (isValid) {
                    if (isValid) {
                        users.get(query)
                            .then(function (data) {
                                var user = data.pop();
                                var hashedPW = hashPassword(query.password, user.passwordsalt);
                                query.password = hashedPW;
                                users.update(query)
                                    .then(function (data) {
                                        tokens.delete(query).then();
                                        var body = data.pop();
                                        res.status(202).send({
                                            email: body.email
                                        })
                                    })
                            })
                    } else {
                        tokens.delete(query).then();
                        res.status(401).send('token expired');
                    }
                },
                function (error) {
                    handleError(error, res);
                });
    }

    function validatePassword(userData, password, res) {
        var successMessage = "Authentication succeeded.";
        var failureMessage = "Authentication failed.";
        if (!userData) {
            res.status(401).send(failureMessage);
        }

        var hashedPW = hashPassword(password, userData.passwordsalt);
        if (hashedPW != userData.password) {
            res.status(401).send(failureMessage);
        }
        else {

            var authCookie = {token: generateToken(), uid: userData.id};
            res
                .cookie('auth', authCookie, {httpOnly: true})
                .status(200)
                .send(successMessage);
        }
    }

    function hashPassword(password, salt) {
        var hash = crypto.createHash('sha256');
        hash.update(password + salt);
        return hash.digest('hex');
    }

    function generateToken() {
        var length = 24;
        var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var token = '';

        for (var i = 0; i < length; i++) {
            token += chars.charAt(Math.floor(Math.random() * (chars.length - 0)));
        }

        return token;
        ;
    }
})();