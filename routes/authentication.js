(function () {
    'use strict';

    module.exports = function checkAuthentication(req, res, next) {

        if (!req.cookies.auth) {
            res.status(401).send("Authentication required").end();
            return;
        }
        else {
            next();
        }
    };

})();