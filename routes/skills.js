(function () {
    'use strict';

    var negotiate = require('./content_negotiation');
    var authenticate = require('./authentication');
    var crypto = require('crypto');
    var router = require('express').Router();
    var handleError = require('../common/errorHandler');

    var Skills = require('../data/Skills');
    var SkillsStats = require('../data/SkillsStats');

    var skills = new Skills();
    var skillsStats = new SkillsStats();

    module.exports = router;

    var routeMap = [
        {path:'/all', methods:{get : skills.allSkills}},
        {path:'/ranks', methods:{get : skills.allRanks}},
        {path:'/categories', methods:{get : skills.allCategories}},

        {path:'/topten', methods:{get : skillsStats.topTen}},
        {path:'/bottomten', methods:{get : skillsStats.bottomTen}},
    ];

    routeMap.forEach(function(routeInfo){
        router.route(routeInfo.path)
            .all(authenticate)
            .all(negotiate)
            .get(function(req, res){
                routeInfo.methods.get()
                .then(function (data) {
                    res
                        .status(200)
                        .send(data);
                }, handleError)
            })
    });

    // TODO: Eliminate duplication?

    router.route('/')
        .all(authenticate)
        .all(negotiate)
        .post(addNew);

    router.route('/:id')
        .all(authenticate)
        .all(negotiate)
        .get(mySkills)
        .post(save)
        .put(save);

    /*==================================================================*/

    function mySkills(req, res) {
        if (req.params.id === 'mine') {
            var id = req.cookies.auth.uid
        }
        else {
            var id = req.params.id;
        }
        skills.get({id: id})
            .then(function (data) {
                if (data.length === 0) {
                    res.status(404)
                        .send();
                }
                if (!data[0].skill_id) {
                    res.status(200)
                        .send([]);
                }
                var body = data.map(function (item) {
                    return {
                        skill_id: item.skill_id,
                        rank_id: item.rank_id,
                        category: item.category,
                        skillName: item.skill,
                        rank: item.rank
                    }
                });
                res
                    .status(200)
                    .send(body);
            }, handleError);
    }

    function save(req, res) {
        var currentUserId = req.cookies.auth.uid;

        if (req.params.id !== 'mine' && req.params.id !== currentUserId) {
            res.status(403).send();
        }
        var ranked_skill = {
            profile_id: currentUserId,
            skill: req.body.skill,
            skill_id: req.body.skill_id,
            rank_id: req.body.rank_id
        }

        skills.save(ranked_skill)
            .then(function (data) {
                var body = data.pop();
                res.status(202)
                    .end();

            }, handleError);
    }

    function addNew(req, res) {
        var skillArray = req.body;

        skillArray.forEach(function (skill) {
            skills.addNew(skill)
                .then(function (data) {
                    },
                    handleError);
        });
        res.status(202)
            .end();
    }
})();