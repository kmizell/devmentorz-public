(function () {
    'use strict';

    var negotiate = require('./content_negotiation');
    var authenticate = require('./authentication');
    var crypto = require('crypto');
    var router = require('express').Router();
    var handleError = require('../common/errorHandler');

    var UserProfiles = require('../data/Profiles');

    var profiles = new UserProfiles();

    module.exports = router;

    router.route('/:id')
        .all(authenticate)
        .all(negotiate)
        .get(getProfile);

    function getProfile(req, res) {
        if (req.params.id === 'mine') {
            var id = req.cookies.auth.uid
        }
        else {
            var id = req.params.id;
        }

        var query = {id: id};

        profiles.get(query)
            .then(function (data) {
                var body = data.pop() || {firstname: "", lastname: ""};
                res
                    .status(200)
                    .send({
                        firstName: body.firstname,
                        lastName: body.lastname
                    });
            }, handleError)
    }
})();