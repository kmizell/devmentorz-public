(function () {
    'use strict';

    var sendgrid  = require('sendgrid')(process.env.SENDGRID_USERNAME, process.env.SENDGRID_PASSWORD);
    var InvitationMessage = require('./InvitationMessage.js');

    module.exports = MailService;

    function MailService(){
        this.sendInvite = sendInvite;
    }

    function sendInvite(body){
        var message = new InvitationMessage(body);

        sendgrid.send(new sendgrid.Email(message), handleError);
    }

    function handleError(err, json){
        if (err) {
            return console.error(err);
        }
        console.log(json);
    }

})();