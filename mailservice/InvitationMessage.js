(function () {
    'use strict';

    module.exports = Message;


    function Message(body) {
        var url = process.env.HOST + "/confirmAccount/" + body.token;

        this.from = "mibarra@aimconsulting.com";
        this.fromname = "Michael Ibarra";
        this.to = [body.email];
        this.subject = 'Confirm your account';
        this.text = textMessage();
        this.html = htmlMessage();

    function textMessage() {
        var text = "You're almost done. Just click on the link below to confirm your account./n/n" + url;

        return text;
    }

    function htmlMessage() {
        var html =
            "<p>You're almost done. Just click on the link below to confirm your account.</p>" +
            "<p><a href='" + url + "'>" + url + "</a></p>"

        return html;
    }

}
})();