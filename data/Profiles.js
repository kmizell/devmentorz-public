(function () {
    'use strict';

    var execute = require('./executeQuery');

    module.exports = Profiles;

    function Profiles(){
        this.get = fetch;
        this.create = post;
        this.update = put;

        return this;
    }

    function fetch(queryParams) {
        var query = {
            text: 'SELECT * FROM profile where ' +
            'id = $1',
            values: [queryParams.id]
        };

        return execute(query);
    }

    function post(body) {
        var query = {
            text: 'INSERT INTO profile ' +
            '(id, firstname, lastname) ' +
            'values '+
            '($1, $2, $3) returning *',
            values: [body.id, body.firstname, body.lastname]
        };

        return execute(query);
    }

    function put(body) {
        var query = {
            text: 'UPDATE profile SET firstname = $1, lastname = $2 WHERE id = $3 returning *',
            values: [body.firstname, body.lastname, body.id]
        };

        return execute(query);
    }
})();