(function () {

    var execute = require('./executeQuery');
    var queries = require('./queries.js');

    module.exports = SkillsStats;

    function SkillsStats(){

        this.topTen = topTenOrgSkills;
        this.bottomTen = bottomTenOrgSkills;

        return this;
    }

    function topTenOrgSkills(){
        return execute(queries.top_ten_org_skills);
    }

    function bottomTenOrgSkills(){
        return execute(queries.bottom_ten_org_skills);
    }

})();