(function () {
    'use strict';

    var execute = require('./executeQuery');

    module.exports = Search;

    function Search(){
        this.query = query;

        return this;
    }

    function query(queryParams) {
        var query = {
            text: 'select * from profile p ' +
            'join ranked_skill rs on p.id = rs.profile_id ' +
            'join skill s on rs.skill_id = s.id ' +
            'join rank r on rs.rank_id = r.id ' +
            'where skill_id = $1 ' +
            'and rank_id >= $2' +
            'order by rank_id desc;',
            values: [queryParams.skill, queryParams.rank]
        };

        return execute(query);
    }
})();