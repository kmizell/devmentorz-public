create table app_users (id serial PRIMARY KEY, email text UNIQUE NOT NULL, password text NOT NULL, passwordsalt text NOT NULL, app text NOT NULL);
create table app_tokens (id serial PRIMARY KEY, email text NOT NULL, token text NOT NULL, app text NOT NULL, expires timestamp NOT NULL);
create table profile(id integer PRIMARY KEY REFERENCES app_users (id), firstName text NOT NULL, lastName text NOT NULL);
create table skill_category (id serial PRIMARY KEY, category text UNIQUE NOT NULL);
create table skill (id serial PRIMARY KEY, skill text UNIQUE NOT NULL, category_id int DEFAULT 0 REFERENCES skill_category(id));
create table rank (id int PRIMARY KEY, rank text UNIQUE NOT NULL);
create table ranked_skill (profile_id integer NOT NULL REFERENCES profile (id), skill_id integer NOT NULL REFERENCES skill(id), rank_id integer NOT NULL REFERENCES rank (id), PRIMARY KEY(profile_id, skill_id));

create view vw_organization_ranked_skills as select category, skill, r.id as rank_id, rank from ranked_skill rs join skill s on rs.skill_id = s.id join rank r on rs.rank_id = r.id order by skill;

insert into rank (id, rank) values (0, 'No Interest');
insert into rank (id, rank) values (5, 'Want To Learn');
insert into rank (id, rank) values (10, 'Beginner');
insert into rank (id, rank) values (15, 'Novice');
insert into rank (id, rank) values (20, 'Intermediate');
insert into rank (id, rank) values (25, 'Advanced');
insert into rank (id, rank) values (30, 'Expert');

insert into skill_category (id, category) values (0, 'Uncategorized');
insert into skill_category (id, category) values (1, 'Languages');
insert into skill_category (id, category) values (2, 'Platforms');
insert into skill_category (id, category) values (3, 'Tools');
insert into skill_category (id, category) values (4, 'Practices');

insert into skill (category_id, skill) values (1, 'C#');
insert into skill (category_id, skill) values (1, 'Ruby');
insert into skill (category_id, skill) values (1, 'Java');
insert into skill (category_id, skill) values (1, 'Python');
insert into skill (category_id, skill) values (1, 'Javascript');

insert into skill (category_id, skill) values (2, 'Windows');
insert into skill (category_id, skill) values (2, 'AWS');
insert into skill (category_id, skill) values (2, 'OpenStack');
insert into skill (category_id, skill) values (2, 'Linux');

insert into skill (category_id, skill) values (3, 'Git');
insert into skill (category_id, skill) values (3, 'Visual Studio');
insert into skill (category_id, skill) values (3, 'Jenkins');
insert into skill (category_id, skill) values (3, 'jUnit');

insert into skill (category_id, skill) values (4, 'TDD');
insert into skill (category_id, skill) values (4, 'Scrum');
insert into skill (category_id, skill) values (4, 'RESTful API Design');
insert into skill (category_id, skill) values (4, 'Domain Modeling');

insert into ranked_skill(profile_id, skill_id, rank_id) values (1, 1, 20);
insert into ranked_skill(profile_id, skill_id, rank_id) values (1, 6, 20);
insert into ranked_skill(profile_id, skill_id, rank_id) values (1, 10, 20);
insert into ranked_skill(profile_id, skill_id, rank_id) values (1, 14, 20);