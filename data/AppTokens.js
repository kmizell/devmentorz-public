(function () {
    'use strict';

    var execute = require('./executeQuery');

    module.exports = AppTokens;

    function AppTokens() {
        this.create = upsert;
        this.validate = validate;
        this.delete = _delete;
    }

    function _delete(appToken) {
        var query = {
            text: 'DELETE FROM app_tokens ' +
            'WHERE email = $1 ' +
            'AND app = $2',
            values: [appToken.email, appToken.app]
        };

        return execute(query);
    }

    function insert(appToken) {
        var query = {
            text: 'INSERT INTO app_tokens ' +
            '(email, token, app, expires) ' +
            'values ' +
            '($1, $2, $3, $4) returning *',
            values: [appToken.email, appToken.token, appToken.app, appToken.expires]
        };

        return execute(query);
    }

    function upsert(appToken) {
        return exists(appToken)
            .then(function (exists) {
                if (exists) {
                    return update(appToken);
                }
                else {
                    return insert(appToken);
                }
            });
    }

    function validate(appToken) {
        var now = new Date(Date.now());
        var query = {
            text: 'SELECT COUNT(*) FROM app_tokens where ' +
            'email = $1 AND ' +
            'app = $2 AND ' +
            'token = $3 AND ' +
            'expires > $4',
            values: [appToken.email, appToken.app, appToken.token, now]
        };

        return execute(query)
            .then(function (data) {
                return data.pop().count > 0;
            });
    }

    function exists(appToken) {
        var query = {
            text: 'SELECT COUNT(*) FROM app_tokens where ' +
            'email = $1 AND ' +
            'app = $2',
            values: [appToken.email, appToken.app]
        };
        return execute(query)
            .then(function (data) {
                return data.pop().count > 0;
            });
    }


    function update(appToken) {
        var query = {
            text: 'UPDATE app_tokens ' +
            'SET token = $1, expires = $2' +
            'WHERE email = $3 AND app = $4' +
            'returning *',
            values: [appToken.token, appToken.expires, appToken.email, appToken.app]
        };

        return execute(query);
    }
})();