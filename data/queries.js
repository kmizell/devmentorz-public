(function () {
    'use strict';

    var queries = {};

    module.exports = queries;

    // Top Org Skills
    queries.top_ten_org_skills =
        'select s.skill, sum(rank_id) as rank from ranked_skill rs join skill s on rs.skill_id = s.id group by s.skill order by Rank desc, skill limit 10;';
    queries.top_ten_org_languages =
        'select s.skill, sum(rank_id) as rank from ranked_skill rs join skill s on rs.skill_id = s.id where s.category_id = 1 group by s.skill order by Rank desc, skill limit 10;';
    queries.top_ten_org_platforms =
        'select s.skill, sum(rank_id) as rank from ranked_skill rs join skill s on rs.skill_id = s.id where s.category_id = 2 group by s.skill order by Rank desc, skill limit 10;';
    queries.top_ten_org_tools =
        'select s.skill, sum(rank_id) as rank from ranked_skill rs join skill s on rs.skill_id = s.id where s.category_id = 3 group by s.skill order by Rank desc, skill limit 10;';
    queries.top_ten_org_practices =
        'select s.skill, sum(rank_id) as rank from ranked_skill rs join skill s on rs.skill_id = s.id where s.category_id = 4 group by s.skill order by Rank desc, skill limit 10;';

    // Bottom Org Skills
    queries.bottom_ten_org_skills =
        'select s.skill, rs.skill_id, round(avg(rs.rank_id)) as avg, count(rs.skill_id) as count from ranked_skill rs join skill s on rs.skill_id = s.id where rank_id > 0 AND rank_id <= 15 group by (rs.skill_id, s.skill) order by count desc, avg limit 10;';

    // Top Individual Skills
    queries.top_ten_ind_skills =
        'select s.skill, sum(rank_id) as rank from ranked_skill rs join skill s on rs.skill_id = s.id where rs.profile_id = $1 group by s.skill order by Rank desc, skill limit 10;';

    // Bottom Individual Skills
    queries.bottom_ten_ind_skills =
        'select s.skill, sum(rank_id) as rank from ranked_skill rs join skill s on rs.skill_id = s.id where rs.profile_id = $1 AND rs.rank_id > 0 group by s.skill order by Rank, skill limit 10;';

})();