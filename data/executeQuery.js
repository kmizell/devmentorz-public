(function () {

    'use strict';

    var q = require('q');
    var pg = require('pg');

    module.exports = executeQuery;

    function executeQuery(query){
        var deferred = q.defer();
        pg.connect(process.env.DATABASE_URL, function (err, client, done) {
            client.query(query, function (err, result) {
                done();
                if (err) {
                    deferred.reject(err);
                }
                else {
                    deferred.resolve(result.rows);
                }
            });
        });

        return deferred.promise;
    }

})();