(function () {
    'use strict';

    var execute = require('./executeQuery');

    module.exports = Skills;

    function Skills(){
        this.get = fetch;
        this.save = save;
        this.addNew = addNew;

        this.allSkills = allSkills;
        this.allRanks = allRanks;
        this.allCategories = allCategories;

        return this;
    }

    function allSkills(){
        var query = {
            text: 'SELECT s.*, c.category ' +
            'FROM skill s ' +
            'JOIN skill_category c on s.category_id = c.id ' +
            'ORDER BY s.skill'
        };
        return execute(query);
    }

    function allRanks(){
        var query = {
            text: 'SELECT * FROM rank'
        };
        return execute(query);
    }

    function allCategories(){
        var query = {
            text: 'SELECT * FROM skill_category'
        };
        return execute(query);
    }

    function fetch(queryParams) {
        var query = {
            text: 'SELECT p.id, rs.skill_id, s.skill, c.category, rs.rank_id, r.rank ' +
            'FROM ranked_skill rs ' +
            'JOIN skill s ON rs.skill_id = s.id ' +
            'JOIN skill_category c ON s.category_id = c.id ' +
            'JOIN rank r ON rs.rank_id = r.id  ' +
            'RIGHT OUTER JOIN profile p ON rs.profile_id = p.id WHERE ' +
            'p.id = $1 ' +
            'ORDER BY c.id, r.id DESC, s.skill',
            values: [queryParams.id]
        };

        return execute(query);
    }

    function save(ranked_skill) {
        return exists(ranked_skill)
            .then(function (exists) {
                if (exists) {
                    return update(ranked_skill);
                }
                else {
                    if (ranked_skill.skill_id === 0){
                        console.log(ranked_skill);
                        var newSkill = {skill: ranked_skill.skill, category_id: 0};
                        return addNew(newSkill)
                            .then(function(result){
                                ranked_skill.skill_id = result.pop().id;
                                return insert(ranked_skill);
                            })
                    }
                    else{
                        return insert(ranked_skill);
                    }
                }
            });
    }

    function addNew(skill){
        var query = {
            text: 'INSERT INTO skill ' +
            '(category_id, skill) ' +
            'values '+
            '($1, $2) returning *',
            values: [skill.category_id, skill.skill]
        };

        return execute(query);
    }

    function insert(ranked_skill) {
        var query = {
            text: 'INSERT INTO ranked_skill ' +
            '(profile_id, skill_id, rank_id) ' +
            'values '+
            '($1, $2, $3) returning *',
            values: [ranked_skill.profile_id, ranked_skill.skill_id, ranked_skill.rank_id]
        };

        return execute(query);
    }

     function update(ranked_skill) {
        var query = {
            text: 'UPDATE ranked_skill SET rank_id = $3 where profile_id = $1 AND skill_id = $2 returning *',
            values: [ranked_skill.profile_id, ranked_skill.skill_id, ranked_skill.rank_id]
        };

        return execute(query);
    }

    function exists(ranked_skill) {
        var query = {
            text: 'SELECT COUNT(*) FROM ranked_skill where ' +
            'profile_id = $1 AND ' +
            'skill_id = $2',
            values: [ranked_skill.profile_id, ranked_skill.skill_id]
        };
        return execute(query)
            .then(function (data) {
                return data.pop().count > 0;
            });
    }
})();