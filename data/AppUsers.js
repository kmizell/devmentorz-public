(function () {
    'use strict';

    var execute = require('./executeQuery');

    module.exports = AppUsers;

    function AppUsers(){
        this.get = fetch;
        this.create = post;
        this.update = put;

        return this;
    }

    function fetch(appUser) {
        var query = {
            text: 'SELECT * FROM app_users where ' +
            'email = $1 AND ' +
            'app = $2',
            values: [appUser.email, appUser.app]
        };

        return execute(query);
    }

    function post(appUser) {
        var query = {
            text: 'INSERT INTO app_users ' +
            '(email, password, passwordsalt, app) ' +
            'values '+
            '($1, $2, $3, $4) returning *',
            values: [appUser.email, appUser.password, appUser.passwordsalt, appUser.app]
        };

        return execute(query);
    }

    function put(appUser) {
        var query = {
            text: 'UPDATE app_users SET password = $1 WHERE email = $2 AND app = $3 returning *',
            values: [appUser.password, appUser.email, appUser.app]
        };

        return execute(query);
    }
})();